<?php

// Set cURL Handler

$ch = curl_init();
const url = "https://restful-booker.herokuapp.com/auth";
const body_data = array(
    'username' => 'admin',
    'password' => 'password123'
);

// convert to JSON

$json_body_data = json_encode(body_data);

// Set Curl URL

curl_setopt($ch, CURLOPT_URL, url);

// making POST Request
curl_setopt($ch, CURLOPT_POST, true);

// POST Body

curl_setopt($ch, CURLOPT_POSTFIELDS, $json_body_data);

// set Header

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-type:application/json' // remember its just array.. not associative array /dictionary
));

// return transfer

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Executing cURL

$response = curl_exec($ch);

// Checking Error

if(curl_errno($ch)){
    echo "Error: ". curl_errno($ch);
}

// decoding json response

$token = json_decode($response)->token;

// Print Token
echo $token;

// closing cURL & Save Resource

curl_close($ch);