<?php


class CurlApiClient {
    private object $curl;
    private $url;
    private $headers;
    private $data;
    
    public function __construct($url) {
        $this->curl = curl_init();
        $this->url = $url;
        $this->headers = array();
        $this->data = array();
        
        curl_setopt($this->curl, CURLOPT_URL, $this->url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_POST, true);
    }
    
    public function setHeader($header) {
        $this->headers[] = $header;
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
    }
    
    public function setData($data) {
        $this->data = $data;
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($this->data));
    }
    
    public function execute() {
        $response = curl_exec($this->curl);
        
        if(curl_error($this->curl)) {
            $error_msg = curl_error($this->curl);
            // Handle the error
        } else {
            $decoded_response = json_decode($response, true);
            // Do something with the response data
        }
        
        curl_close($this->curl);
    }
}

// Usage example
$url = "https://example.com/api";
$client = new CurlApiClient($url);

$client->setHeader('Content-Type: application/json');
// $client->setHeader('Authorization: Bearer abcdef123456');
$client->setData(array(
    'param1' => 'value1',
    'param2' => 'value2'
));

$client->execute();

