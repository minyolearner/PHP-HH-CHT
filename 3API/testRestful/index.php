<?php

// Set the endpoint URLs
$auth_url = 'https://restful-booker.herokuapp.com/auth';
$booking_url = 'https://restful-booker.herokuapp.com/booking';

// Set the authentication credentials
$username = 'admin';
$password = 'password123';

// Set the booking data
$booking_data = array(
    'firstname' => 'John',
    'lastname' => 'Doe',
    'totalprice' => 123,
    'depositpaid' => true,
    'bookingdates' => array(
        'checkin' => '2022-04-01',
        'checkout' => '2022-04-05'
    ),
    'additionalneeds' => 'Breakfast'
);

// Encode the booking data as JSON
$booking_data_json = json_encode($booking_data);

// Set up the authentication request
$auth_request = curl_init($auth_url);
curl_setopt($auth_request, CURLOPT_POST, true);
curl_setopt($auth_request, CURLOPT_POSTFIELDS, "username=$username&password=$password");
curl_setopt($auth_request, CURLOPT_RETURNTRANSFER, true);

// Make the authentication request and get the token
$auth_response = curl_exec($auth_request);
$token = json_decode($auth_response)->token;

// Set up the booking request with the token in the Authorization header
$booking_request = curl_init($booking_url);
curl_setopt($booking_request, CURLOPT_POST, true);
curl_setopt($booking_request, CURLOPT_POSTFIELDS, $booking_data_json);
curl_setopt($booking_request, CURLOPT_RETURNTRANSFER, true);
curl_setopt($booking_request, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "Authorization: Bearer $token"
));

// Make the booking request
$booking_response = curl_exec($booking_request);

// encode to array

$booking_response_json = json_decode($booking_response,true);
echo $booking_response_json['bookingid'];


echo "</br>";


// Print the booking response
echo $booking_response;
