<?php

namespace minyo\ComplexCurl;

interface CurlApi{

    public function setHeader(array $data = [],bool $json);
    public function setBody(array $data);
    public function execute();
    public static function void():string;

}

trait CurlProperty{
    private object $curl;
    private string $url;
    private array $header;
    private array $body;
    private bool $json;
}

class CurlApiPost implements CurlApi{

    use CurlProperty;
    
    public function __construct(string $url){
        $this->curl = curl_init();
        $this->url = $url;

        curl_setopt($this->curl,CURLOPT_POST, true);
        curl_setopt($this->curl,CURLOPT_URL, $this->url);
        curl_setopt($this->curl,CURLOPT_RETURNTRANSFER, true);
    }

    public function setHeader(array $data = [], bool $json){
        $this->header = $data;
        $this->json = $json;
        if($this->header == []){
            return;
        }else{
            curl_setopt($this->curl,CURLOPT_HTTPHEADER,$this->header);
        }
    }
    public function setBody(array $data,){
        $this->body = $data;

        if($this->json){
            curl_setopt($this->curl,CURLOPT_POSTFIELDS,json_encode($this->body));
        }else{
            curl_setopt($this->curl,CURLOPT_POSTFIELDS,"username=admin&password=password123");
        }
    }
    public function execute(){
        // Execute
        $response = curl_exec($this->curl);
        // Print Value & Error Check

        if(curl_errno($this->curl)){
            echo "Error: ". curl_errno($this->curl); 
        }else{
            if($this->json){
                $token = json_decode($response)->token;
                echo $token;
            }else{
                echo $response;
            }
        }
    
    }

    public static function void():string{
        return 'striong';
    }
}