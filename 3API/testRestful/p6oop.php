<?php
// Declare Namespace

class CurlApiClient{

    // Intilaize Property

    private object $curl;
    private string $url;
    private array  $header;
    private array  $data;

    // Constructio
    public function __construct(string $url){
        $this->curl = curl_init();
        $this->url = $url;

        
        curl_setopt($this->curl,CURLOPT_URL,$this->url);
        curl_setopt($this->curl,CURLOPT_POST,true);
        curl_setopt($this->curl,CURLOPT_RETURNTRANSFER,true);
    }
    // Set Header
    public function setHeader(array $data){
        $this->header = $data;
        curl_setopt($this->curl,CURLOPT_HTTPHEADER,$this->header);
    }
    // Set BodyData
    public function bodyData(array $data){
        $this->data = $data;
        curl_setopt($this->curl,CURLOPT_POSTFIELDS,json_encode($this->data));

    }
    // Execute & Error Check
    public function Execute(){


        // Execute
        $response = curl_exec($this->curl);
        // Check Error
        if(curl_errno($this->curl)){
            echo "Error :" , curl_errno($this->curl);
        }else{
            $token = json_decode($response)->token;
        }
        // Close Connection
        curl_close($this->curl);

        return $token;
    }


}

$ReqOne = new CurlApiClient("https://restful-booker.herokuapp.com/auth");

$ReqOne->setHeader(array("Content-Type: application/json"));

$ReqOne->bodyData(array(
    "username" => "admin",
    "password" => "password123"
));

echo $ReqOne->Execute();