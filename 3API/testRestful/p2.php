<?php


// Handler for API

$ch = curl_init();

$payload = array(
    "username" => "admin",
    "password" => "password123"
);

$payload = json_encode($payload);

curl_setopt($ch, CURLOPT_URL, "https://restful-booker.herokuapp.com/auth");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
));
// curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$response = curl_exec($ch);

if (curl_errno($ch)) {
    echo "Error: ". curl_error($ch);
}

echo $response;

curl_close($ch);