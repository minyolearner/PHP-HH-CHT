<?php

// Set the endpoint URLs
$auth_url = 'https://restful-booker.herokuapp.com/auth';

// Set the authentication credentials
$username = 'admin';
$password = 'password123';

// Set up the authentication request
$ch = curl_init($auth_url);

curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, "username=$username&password=$password");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Make the authentication request and get the token
$auth_response = curl_exec($ch);
$token = json_decode($auth_response)->token;

echo "Token: $token\n";