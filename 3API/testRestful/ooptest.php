<?php
// Declare Namespace

class CurlApiClient{

    // Intilaize Property

    private $curl;
    private $url;
    private $header;
    public $data;
    

    // Constructio
    public function __construct($url){
        $this->curl = curl_init();
        $this->url = $url;
        $this->header = array();
        $this->data = array();

        
        curl_setopt($this->curl,CURLOPT_URL,$this->url);
        curl_setopt($this->curl,CURLOPT_POST,true);
        curl_setopt($this->curl,CURLOPT_RETURNTRANSFER,true);
    }
    // Set Header
    // public function setHeader($data){
    //     $this->header = $data;
    // }
    // Set BodyData
    public function bodyData($data){
        $this->data = $data;

    }
    // Execute & Error Check
    public function Execute(){
        curl_setopt($this->curl,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));

        $jdata = json_encode($this->data);
        curl_setopt($this->curl,CURLOPT_POSTFIELDS, $jdata);
        echo $jdata;


        // Execute
        $response = curl_exec($this->curl);
        // Check Error
        if(curl_errno($this->curl)){
            echo "Error :" , curl_errno($this->curl);
        }else{
            echo $response;
        }
        // Close Connection
        curl_close($this->curl);
    }


}

$ReqOne = new CurlApiClient("https://restful-booker.herokuapp.com/auth");

// $ReqOne->setHeader("Content-Type: application/json");

$ReqOne->bodyData(array(
    "username" => "admin",
    "password" => "password123"
));

$ReqOne->Execute();