<?php

// set cURL Handler

$ch = curl_init();

$url = 'https://restful-booker.herokuapp.com/auth';

$post_body_data = array(
    'username' => 'admin',
    'password' => 'password123'
);

$post_body_data = json_encode($post_body_data);

// set curl options

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-type:application/json',
));
curl_setopt($ch, CURLOPT_POSTFIELDS,$post_body_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// execute curl

$response = curl_exec($ch);

$token = json_decode($response)->token;

if(curl_errno($ch)){
    echo "Error:". curl_errno($ch);
}

// print value

echo $token; // response must be in JSON

// curl Close

curl_close($ch);