<?php

namespace dir\minyo;

interface curlBone{
    public function setHeader(array $data):null;
    public function setBody(array $data):null;
    public function getToken():null;
    public function getBookID():array;
    public function getBookingInfo(string $data):null;
    public function setBooking(string $data):null;
    public function updateBooking(string $data ):null;
    public function deleteBooking(string $data):null;
    public static function health(string $url):string;
    public static function void(string $data):null;
}
trait curlProperty{
    private object $curl;
    private string $url;
    private array  $header;
    private array  $body;
    private string $token;
    private string $bookid_url;
    private array  $BookingID;
    private array  $BookingInfo;
}
class curl implements curlBone{
    use curlProperty;

    public function __construct(string $url){
        $this->curl = curl_init();
        $this->url = $url;

        curl_setopt($this->curl,CURLOPT_POST,true);
        curl_setopt($this->curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->curl,CURLOPT_URL,$this->url);

        return 0;
    }

    public function setHeader(array $data):null{
        $this->header = $data;
        curl_setopt($this->curl,CURLOPT_HTTPHEADER,$this->header);


        return null;
    }

    public function setBody(array $data):null{
        $this->body = $data;
        curl_setopt($this->curl,CURLOPT_POSTFIELDS,json_encode($this->body));

        return null;
    }

    public function getToken():null{
        // Exec Code
        $response = curl_exec($this->curl);

        if(curl_errno($this->curl)){
            echo "Error: ". curl_errno($this->curl);
        }else{
            $this->token = json_decode($response)->token;
        }

        return null;
    }

    public function getBookID():array{
    
        $booking_request = curl_init($booking_url);
        curl_setopt($booking_request, CURLOPT_POST, true);
        curl_setopt($booking_request, CURLOPT_POSTFIELDS, $booking_data_json);
        curl_setopt($booking_request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($booking_request, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Bearer $token"
        ));


        
    }

    public function getBookingInfo(string $data):null{

    }
    public function setBooking(string $data):null{

    }
    public function updateBooking(string $data ):null{

    }
    public function deleteBooking(string $data):null{

    }
    public static function health(string $url):string{

    }
    public static function void(string $data):null{

    }


}


$ReqOne = new curl("https://restful-booker.herokuapp.com/auth");

$ReqOne->setHeader(array("Content-Type: application/json"));

$ReqOne->setBody(array(
    "username" => "admin",
    "password" => "password123"
));

$ReqOne->getToken();

echo $ReqOne->token;