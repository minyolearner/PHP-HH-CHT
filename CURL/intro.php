<?php

// intialize cURL Session

$ch = curl_init();

// Set cURL Options

curl_setopt($ch,CURLOPT_URL,"http://google.com/");

curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); // that means we're returning a string rather than an array

// Setting Up User Agent
curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");

// This option verifies the SSL/TLS certificate of the HTTPS connection.
curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);

//This option verifies that the hostname in the SSL/TLS certificate matches the hostname of the URL being requested.
curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);


// Follow HTTP redirect 301 or 302
curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);

// Set Max Redirect Times - Defaults 20
// CURLOPT_MAXREDIRS is a cURL option in PHP that specifies the maximum number of HTTP redirects that cURL will follow. When a cURL request is redirected more than the specified number of times, the request will be stopped and an error will be returned.
curl_setopt($ch,CURLOPT_MAXREDIRS,10);

// CURLOPT_TIMEOUT is a cURL option in PHP that sets the maximum time in seconds that cURL is allowed to spend on a request before timing out. If the server does not respond within the specified timeout period, the request will be aborted and an error will be returned.
curl_setopt($ch,CURLOPT_TIMEOUT,10);

// curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,10);



// execute  

$response = curl_exec($ch);

// echo response

echo $response;