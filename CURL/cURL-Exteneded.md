# Post Request

`CURLOPT_POST` used for making post request.A Nice Example Below.

```php

$options = array(
    CURLOPT_URL => 'http://example.com',
    CURLOPT_POST => true, // use HTTP POST method
    CURLOPT_POSTFIELDS => 'name=John&age=30', // set POST data or Request Body fields
    CURLOPT_RETURNTRANSFER => true,
);

//init,set options, execute & close connection

$curl = curl_init();
curl_setopt_array($curl, $options);
$response = curl_exec($curl);
curl_close($curl);

```

# Setting Header Authorization - Content-Type

`CURLOPT_HTTPHEADER` used for custom HTTP header. Example Below.

```PHP

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: '. strlen(json_encode(array('foo' => 'bar'))),
    'Connection: close',
));

```

# Request Body Authorization

`CURLOPT_POSTFIELDS` used for POST request body(Must Be in JSON). Example Below.

```php
$options = array(
CURLOPT_POSTFIELDS => 'name=John&age=30', // set POST data or Request Body fields
);
```

### Another Example Request Body Authorization

```php

// Set POST data
$data = array(
    'username' => 'admin',
    'password' => 'password123'
);
$postData = json_encode($data);

// Set cURL options
$url = 'https://restful-booker.herokuapp.com/auth';
$headers = array(
    'Content-Type: application/json'
);
$options = array(
    CURLOPT_URL => $url,
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => $postData, // Must be in JSON format and must contain POST data
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => $headers,
    CURLOPT_SSL_VERIFYPEER => false, // Only use for development or testing environments
);



```
# cURL Error & HTTP Status Code

```php

// Initialize cURL session
$curl = curl_init();
curl_setopt_array($curl, $options);

// Execute the request
$response = curl_exec($curl);

// Check for errors
if(curl_errno($curl)) {
    $error_msg = curl_error($curl);
    echo "cURL error: " . $error_msg;
}

// Get HTTP status code
$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

// Close cURL session
curl_close($curl);

// Output response and status code
echo "Response: " . $response . "\n";
echo "HTTP status code: " . $http_code . "\n";

```