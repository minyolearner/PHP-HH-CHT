# Setting up cURL connection

`curl_init` function should used to initialize  cURL connection

# cURL Options

`curl_setopt()` is a function in PHP's cURL library that is used to set various options for a cURL session. These options modify the behavior of cURL and allow you to customize how HTTP requests are made and processed.

Please check `cURL-Options.md` for more information.

# Execute cURL command

```PHP

curl_exec($handler);

```

# Checking cURL connection status

`CURLINFO_HTTP_CODE` is a constant in cURL that is used to retrieve the HTTP status code of the last request made with the `curl_exec()` function. The HTTP status code is a three-digit numeric code that indicates the status of the HTTP request/response cycle.

When you make a cURL request, the server will respond with an HTTP status code that indicates whether the request was successful or not. Common HTTP status codes include `200 OK` (successful), `404 Not Found` (resource not found), `500 Internal Server Error (server error)`, and many others.

By using the `curl_getinfo()` function with the `CURLINFO_HTTP_CODE` constant, you can retrieve the HTTP status code of the last request made with c`url_exec()`. For example:

```PHP
// Initialize cURL session
$ch = curl_init();

// Set cURL options
curl_setopt($ch, CURLOPT_URL, "https://example.com");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute cURL request
$response = curl_exec($ch);

// Get HTTP status code
$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

// Close cURL session
curl_close($ch);

// Output HTTP status code
echo "HTTP status code: " . $http_code;
```

# Closing a connection

`curl_close()` is a function in PHP's cURL library that is used to close a cURL session and free up system resources. When you use cURL to make HTTP requests, PHP creates a cURL session that manages the request and response. Once the request is completed, it's a good practice to close the cURL session using `curl_close()`.

If you don't close the cURL session, it may remain open and hold onto system resources such as memory, file handles, and network sockets. This can cause performance issues and potentially crash your PHP script if too many unclosed cURL sessions accumulate.

In addition, when you close the cURL session using `curl_close()`, any associated cookies or cached data will also be cleared. This ensures that subsequent cURL requests don't accidentally use stale data or cookies from previous requests.

```php

curl_close($handler);

```
