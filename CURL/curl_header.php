<?php

// set curl handler

$ch = curl_init();

$url = 'http://www.example.com';

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // check HH Tuts

// curl header

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: '. strlen(json_encode(array('foo' => 'bar'))),
    'Connection: close',
));

// curl body


$data = curl_exec($ch);

// check for success

if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
    echo $data;
}

curl_close($ch);