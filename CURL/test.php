<?php

// intialization cURL 7.85.0

$ch = curl_init();

// url
$url = 'https://github.com/';

// set options
curl_setopt($ch , CURLOPT_URL, $url);


// SSL Options not verifying
curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true); // Notice: curl_setopt(): CURLOPT_SSL_VERIFYHOST no longer accepts the value 1, value 2 will be used instead
curl_setopt($ch , CURLOPT_SSL_VERIFYHOST, false); // Notice: curl_setopt(): CURLOPT_SSL_VERIFYHOST no longer accepts the value 1, value 2 will be used instead 


// execute  

$response = curl_exec($ch);

// echo response

echo $response;

?>
